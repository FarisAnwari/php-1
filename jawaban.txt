//1. creating database
CREATE DATABASE myshop;
use myshop;

//2. creating tables
CREATE TABLE users (
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255)
)

CREATE TABLE categories (
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
)

CREATE TABLE items (
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
)

//3. inserting data
INSERT INTO users (
    name, email, password
) VALUES 
(
    'John Doe', 'john@doe.com', 'john123'
),
(
    'Jane Doe', 'jane@doe.com', 'jenita123'
);

INSERT INTO categories (
    name
) VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded');

INSERT INTO items (
    name,
    description,
    price,
    stock,
    category_id
) VALUES
(
    'Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100,
    (SELECT id FROM `categories` WHERE id = 1)
),
(
    'Uniklooh', 'baju keren dari brand ternama', 500000, 50,
    (SELECT id FROM `categories` WHERE id = 2)
),
(
    'IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10,
    (SELECT id FROM `categories` WHERE id = 1)
);

//4. Reading database

SELECT id, name, email FROM users WHERE 1;

//item harga > 1jt
SELECT * FROM items WHERE price > 1000000;

//item nama like uniklo ato watch ato sang (salah satu aja)
SELECT * FROM items WHERE name LIKE '%sang%';

//item join categories
SELECT items.*, categories.name
FROM items
JOIN categories ON items.category_id = categories.id;

//5. update data
UPDATE items
SET price = 2500000 WHERE name = 'Sumsang b50';

/////////////////////////////////////////////
buat esai quiz 2
CREATE TABLE customers (
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255)
)

CREATE TABLE orders (
    id int PRIMARY KEY AUTO_INCREMENT,
    amount int(8),
    customer_id int,
    FOREIGN KEY (customer_id) REFERENCES customers(id)
)

INSERT INTO customers (name, email, password)
VALUES
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO orders (amount, customer_id)
VALUES
(500, (SELECT id FROM customers WHERE id = 1)),
(200, (SELECT id FROM customers WHERE id = 2)),
(750, (SELECT id FROM customers WHERE id = 2)),
(250, (SELECT id FROM customers WHERE id = 1)),
(400, (SELECT id FROM customers WHERE id = 2));

SELECT customers.name AS customer_name, 
(SELECT SUM(orders.amount) FROM orders WHERE orders.customer_id = customers.id)
AS total_amount
FROM customers JOIN orders ON customers.id =  orders.customer_id
GROUP BY name;