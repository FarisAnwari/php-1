<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zoo</title>
</head>

<body>
    <?php
    // require 'animal.php';
    // require 'frog.php';
    // require 'ape.php';
    // $sheep = new Animal("shaun");

    // echo "Name : $sheep->name<br>"; // "shaun"
    // echo "legs : $sheep->legs<br>"; // 4
    // echo "cold blooded : $sheep->cold_blooded<br>"; // "no"
    // echo "<br>";

    // $kodok = new Frog("buduk");
    // echo "Name : $kodok->name<br>"; // buduk
    // echo "legs : $kodok->legs<br>"; // 4
    // echo "cold blooded : $kodok->cold_blooded<br>"; // "no"
    // echo "Jump : ";
    // $kodok->jump(); // "hop hop"
    // echo "<br>";

    // echo "<br>";

    // $sungokong = new Ape("kera sakti");
    // echo "Name : $sungokong->name<br>"; // kera sakti
    // echo "legs : $sungokong->legs<br>"; // 2
    // echo "cold blooded : $sungokong->cold_blooded<br>"; // "no"
    // echo "Yell : ";
    // $sungokong->yell(); // "Auooo"
    // echo "<br>";
    // function tukar_besar_kecil($string)
    // {
    //     $string = strtolower($string) ^ strtoupper($string) ^ $string;
    //     return $string;
    // }

    // // TEST CASES
    // echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    // echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    // echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    // echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    // echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
    // 

    function ubah_huruf($string)
    {
        $temp = array();
        $characters =  str_split($string);
        foreach ($characters as $idx => $char) {
            $result[$idx] = chr(((ord($char) + 1 - 97) % 26) + 97);
        }

        return join("", $result);
    }
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>

</html>